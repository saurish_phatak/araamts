import express from "express";
import { IRouterClass } from "./IRouterClass";

export interface IAraam {
	run(): express.Application;

	appConfig: any;

	instanceMap: Map<string, IRouterClass>;
}