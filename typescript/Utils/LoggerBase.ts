const chalk = require("chalk");

export class LoggerBase {
    // Whether to show 'INFO'
    protected static showInfo: boolean = true;

    // Whether to show 'WARNING'
    protected static showWarning: boolean = true;

    // Whether to show 'ERROR'
    protected static showError: boolean = true;

    // Whether to show params
    protected static showParams: boolean = true;

    // Whether to enable all 
    protected static enableAll: boolean = true;

    // Method Decorator
    public static call() {
        // This function will be executed just once
        return (
            target: any,
            propertyKey: string,
            propertyDescriptor: PropertyDescriptor
        ) => {
            // Preserve the original function definition
            const originalMethod: Function = propertyDescriptor.value;

            // Modify the old function now
            // Give a new function that accepts any argument
            // and print those arguments
            propertyDescriptor.value = function (...varargs: any[]) {
                let newArguments: any[] = [];

                // If the varargs have a function, simply capture its name 
                varargs.forEach(param => {
                    typeof param == 'function' ? (param.name == '' ? newArguments.push('anonymous()') : newArguments.push(`${param.name}()`)) : (newArguments.push(param))
                });

                // This is the new thing that the original function will execute first
                LoggerBase.info(`${target.constructor.name}::${propertyKey}()`, newArguments);

                // Now call original function definition
                return originalMethod.apply(this, varargs);
            }

            // Return the propertyDescriptor
            return propertyDescriptor;
        }
    }

    // Class Decorator
    public static log(targetClassConstructor: Function) {
        LoggerBase.info(`${targetClassConstructor.name}:: constructor()`, []);
    }

    // Decides whether to print info
    public static info(message: string, parameters: any[]) {
        // If the enableAll and showInfo is true
        LoggerBase.enableAll && LoggerBase.showInfo && LoggerBase.message(`${chalk.green.bold('[INFO] ')} ${LoggerBase.time()} ${chalk.blue(message)} ${parameters.length > 0 ? LoggerBase.params(parameters) : ''}`);
    }

    // Prints the given message
    public static message(messageToPrint: string) {
        console.log(messageToPrint);
    }

    // Decides whether to print params
    private static params(parameters: any[]): string {
        return LoggerBase.enableAll ? LoggerBase.showParams ? `Params[${[...parameters]}]` : `` : ``;
    }

    // Returns the time
    private static time(): string {
        const pad = (num: number) => num.toString().padStart(2, "0");

        // Current Date
        let dO: Date = new Date();

        return `[${pad(dO.getHours())}: ${pad(dO.getMinutes())}: ${pad(dO.getMilliseconds())}]`
    }

    // Decides whether to print warning
    public static warn(message: string) {
        // If enableAll and showWarning are true
        if (LoggerBase.enableAll && LoggerBase.showWarning)
            LoggerBase.message(`${chalk.yellowBright.bold('[WARN] ')} ${LoggerBase.time()} ${chalk.yellowBright(message)} `);

        else
            LoggerBase.message(`${chalk.yellowBright('Warning messages are not ON!')} ${chalk.yellowBright.bold('-> Call toggleWarn() method to enable them.')} `);
    }

    // Decides whether to print error
    public static error(message: string, parameters: any[]) {
        if (LoggerBase.enableAll && LoggerBase.showError) {
            LoggerBase.message(`${chalk.redBright.bold('[ERROR]')} ${LoggerBase.time()} ${chalk.redBright(message)} ${[...parameters]} `);
        }
        else
            LoggerBase.message(`${chalk.redBright('Error messages are not ON!')} ${chalk.redBright.bold('-> Call toggleError() method to enable them.')} `)
    }

    // Toggles showWarning
    public static toggleWarning() {
        LoggerBase.showWarning = !LoggerBase.showWarning;
    }

    // Toggles showError
    public static toggleError() {
        LoggerBase.showError = !LoggerBase.showError;
    }

    // Toggles showParams
    public static toggleParams() {
        LoggerBase.showParams = !LoggerBase.showParams;
    }

    // Toggles showInfo
    public static toggleInfo() {
        LoggerBase.showInfo = !LoggerBase.showInfo;
    }

    // Toggles enableAll
    public static toggleAll() {
        LoggerBase.enableAll = !LoggerBase.enableAll;
    }

}
